﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AboutTexts {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AboutTexts() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApplication1.Resources.AboutTexts", typeof(AboutTexts).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda.
        /// </summary>
        public static string about {
            get {
                return ResourceManager.GetString("about", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Önce müşteri prensibiyle çalışan; rahat, şık, iddalı, spor... Sen ne istersen bulabileceksin! Bir ayakkabıdan çok daha fazlası, Ayakkabı Diyarı....
        /// </summary>
        public static string aboutWrap1 {
            get {
                return ResourceManager.GetString("aboutWrap1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Herkes bize yapmamamızı tavsiye etti, çünkü binlerce rakip, soru işaretleri ve bize inanmayanlar vardı, fakat dinlemedik. Ve yaptık Ayakkabı Diyarı artık her yerde!.
        /// </summary>
        public static string aboutWrap2 {
            get {
                return ResourceManager.GetString("aboutWrap2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ayakkabı Diyarı, dünyanın dört bir yanında seninle olan lider bir e-ticaret mağazasıdır..
        /// </summary>
        public static string headerWrap {
            get {
                return ResourceManager.GetString("headerWrap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana Sayfa.
        /// </summary>
        public static string main {
            get {
                return ResourceManager.GetString("main", resourceCulture);
            }
        }
    }
}
