﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models.Controllers
{
    
    public class HomeController : Controller
    {
        
        // GET: Home
        public ActionResult Index()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                ViewData["TopSellers"] = db.Product.Where(x => x.CategoryID == 2).Concat(db.Product.Where(x => x.CategoryID == 1)).ToList();
            }
            return View(ViewData["TopSellers"]);
        }
        public ActionResult Erkek()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                return View(db.Product.Where(x => x.CategoryID == 2).ToList());
            }
        }
        public ActionResult Kadin()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                return View(db.Product.Where(x => x.CategoryID == 1).ToList());
            }
        }
        public ActionResult About()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }
        public ActionResult UrunDetay(int urunID)
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                ViewData["ProductDetail"] = db.Product.Where(x => x.ProductID == urunID).ToList();
                ViewData["Comments"] = db.Comments.Where(x => x.ProductID == urunID).ToList();
            }
            
                return View(ViewData["ProductDetail"]);
        }
        public ActionResult Odeme()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }

        

        [HttpGet]
        public ActionResult Card()
        {
            int user;
            user = Convert.ToInt32(Session["UserID"]);
            
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                ViewData["CartItems"] = db.Cart.Where(x => x.UserID == user).ToList();
                Session["Price"] = 500m; //Burası sepetin total fiyatına eşlenecek.
            }

            
            return View(ViewData["CartItems"]);
        }

        

        [HttpPost]
        public ActionResult Card(string code)
        {

            int user;
            user = Convert.ToInt32(Session["UserID"]);

            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                ViewData["CartItems"] = db.Cart.Where(x => x.UserID == user).ToList();
                
            }

            
            if ((code == "KISINDIRIMI"))
            {
                Session["Price"] = Convert.ToInt32(Session["Price"])-20;
            }


            return View(ViewData["CartItems"]);
        }
        public ActionResult IstekListesi()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }
        public ActionResult OdemeBasarili()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }
        public ActionResult Contact()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }

        public ActionResult SignUp()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }

        public ActionResult LogIn()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
                return View();
        }
    }
}