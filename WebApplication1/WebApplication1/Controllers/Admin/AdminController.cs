﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["TotalPrice"] = db.Product.Select(x => x.price).Sum();
                ViewData["ActiveProducts"] = db.Product.Where(x => x.IsActive==true).Count();
                ViewData["NonActiveProducts"] = db.Product.Where(x => x.IsActive == false).Count();
                ViewData["Products"] = db.Product.ToList();

            }
            return View(ViewData["Products"]);
        }
    }
}