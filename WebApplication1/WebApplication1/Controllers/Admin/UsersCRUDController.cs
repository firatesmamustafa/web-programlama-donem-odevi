﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class UsersCRUDController : Controller
    {
        private AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2();

        // GET: UsersCRUD
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: UsersCRUD/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: UsersCRUD/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UsersCRUD/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,FirstName,LastName,EmailID,DateOfBirth,Password,ConfirmPassword,IsEmailVerified,ActivationCode,isAdmin")] Users users)
        {

            bool Status = false;
            string message = "";

            if (ModelState.IsValid)
            {
                var isExist = IsMailExist(users.EmailID);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist.");
                    return View(users);
                }

                users.ActivationCode = Guid.NewGuid();
                users.Password = Crypto.Hash(users.Password);
                users.ConfirmPassword = Crypto.Hash(users.ConfirmPassword);
                users.IsEmailVerified = true;

                using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
                {
                    db.Users.Add(users);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            return View(users);
        }

        // GET: UsersCRUD/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: UsersCRUD/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,FirstName,LastName,EmailID,DateOfBirth,Password,IsEmailVerified,ActivationCode,isAdmin")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: UsersCRUD/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: UsersCRUD/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [NonAction]

        public bool IsMailExist(string emailID)
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                var v = db.Users.Where(a => a.EmailID == emailID).FirstOrDefault();
                return v != null;
            }
        }
    }
}
