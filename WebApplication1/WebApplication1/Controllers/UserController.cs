﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [AllowAnonymous]
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Registration()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "IsMailVerified, ActivationCode")]Users users)
        {
            bool Status = false;
            string message = "";

            if (ModelState.IsValid)
            {
                var isExist = IsMailExist(users.EmailID);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist.");
                    return View(users);
                }

                users.ActivationCode = Guid.NewGuid();
                users.Password = Crypto.Hash(users.Password);
                users.ConfirmPassword = Crypto.Hash(users.ConfirmPassword);
                users.IsEmailVerified = true;
                users.isAdmin = false;




                using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
                {
                    ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                    ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                    db.Users.Add(users);
                    db.SaveChanges();
                    

                    //SendVerificationEmail(users.EmailID, users.ActivationCode.ToString());
                    message = "Registration succesfully done. Check the " + users.EmailID;
                    Status = true;
                }
            }

            else
            {
                message = "Invalid Request.";
            }
            ViewBag.Message = message;

            ViewBag.Status = Status;
            return View(users);
        }

        public ActionResult LogOut()
        {
            Session["UserName"] = null;
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Auth(Users usr)
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                ViewData["PictureTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "1").ToList();
                ViewData["TextTrademarks"] = db.Advertisement.Where(x => x.AdvCategory == "0").ToList();
                var passCrypted = Crypto.Hash(usr.Password);
                var usrDetail = db.Users.Where(x => x.EmailID == usr.EmailID && x.Password == passCrypted).FirstOrDefault();
                

                if (usrDetail == null)
                {
                    ViewBag.Message = "Login olunamıyor. Bilgilerinizi kontrol edin.";
                    return View();
                }

                else if (usrDetail.isAdmin)
                {
                    
                    FormsAuthentication.SetAuthCookie(usrDetail.EmailID, false);
                    
                    return RedirectToAction("Index", "Admin");
                }

                else
                {
                    Session["UserName"] = usrDetail.FirstName;
                    Session["UserID"] = usrDetail.UserID;
                    FormsAuthentication.SetAuthCookie(usrDetail.EmailID, false);
                    
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        [NonAction]

        public bool IsMailExist(string emailID)
        {
            using (AyakkabiDunyasiDBEntities2 db = new AyakkabiDunyasiDBEntities2())
            {
                var v = db.Users.Where(a => a.EmailID == emailID).FirstOrDefault();
                return v != null;
            }
        }


        //public void SendVerificationEmail(string emailID, string activationCode)
        //{
        //    var verifyURL = "/user/VerifyAccount/" + activationCode;
        //    var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyURL);

        //    var fromEmail = new MailAddress("mustafa.eozmen@gmail.com","Ayakkabı Dünyası");
        //    var toEmail = new MailAddress(emailID);
        //    var fromEmailPassword = "Bindoksan!7";
        //    string subject = "Your account created!";
        //    string body = "Oluşturuldu. Tıklayın: <a href='" + link + "</a>";
        //    var smtp = new SmtpClient
        //    {
        //        Host = "smtp.gmail.com",
        //        Port = 587,
        //        EnableSsl = true,
        //        DeliveryMethod = SmtpDeliveryMethod.Network,
        //        UseDefaultCredentials = false,
        //        Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
        //    };

        //    using (var message = new MailMessage(fromEmail, toEmail)
        //    {
        //        Subject = subject,
        //        Body = body,
        //        IsBodyHtml = true,
        //    })

        //    smtp.Send(message);
        //}
    }

}